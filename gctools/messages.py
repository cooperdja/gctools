BRIGHT_RED     = "\033[31;1m"
BRIGHT_YELLOW  = "\033[33;1m"
BRIGHT_MAGENTA = "\033[35;1m"
MAGENTA        = "\033[35m"
DEFAULT_COLOUR = "\033[0m"    
    
class Task:
    def __init__(self, level: int = 0):
        self.level = level
        self.indent = (3 * self.level) * ' '
                
    def show(self, title: str):
        #prefix = {0: '* ', 1: '-> '}.get(self.level, '')
        colour = {0: BRIGHT_MAGENTA, 1: MAGENTA}.get(self.level, '')        
        print(f'{self.indent}{colour}{title}{DEFAULT_COLOUR}')
        return self
            
    def subtask(self):
        return Task(self.level + 1)

    def warning(self, msg: str):
        print(f'{self.indent}   {BRIGHT_YELLOW}! {msg}{DEFAULT_COLOUR}')
        
    def error(self, msg: str):
        print(f'{self.indent}   {BRIGHT_RED}!! {msg}{DEFAULT_COLOUR}')
