import contextlib
import datetime
import io
import os
import re
import time
import zipfile

PATTERN = re.compile(r"^(?P<assessment>.+)_(?P<username>[^_]+)_attempt_(?P<timestamp>[-0-9]+)(.txt|_?(?P<filename>.+))$")

class InvalidGradebookException(Exception):
    def __init__(self, msg):
        super().__init__("Gradebook doesn't conform to the expected format: " + msg)      
        

class GradeBookFileEntry:
    def __init__(self, zip_entry, zip_reader, meta_file_name):
        self._zip_entry = zip_entry
        self._zip_reader = zip_reader
        self._meta_file_name = meta_file_name
        
        self._match = PATTERN.fullmatch(zip_entry.filename)
        if self._match is None:
            raise InvalidGradebookException("'{}' doesn't seem to be a validly-mangled filename".format(zip_entry.filename))
        
        if self._match['filename'] is None:
            # Metadata
            with zip_reader.open(zip_entry.filename) as reader:
                self._student_name = ' '.join(reader.readline().decode(encoding = 'utf-8').split()[1:-1])
        else:
            self._student_name = None

    @property
    def mangled_name(self):
        return self._zip_entry.filename
        
    @property
    def zip_entry(self):
        return self._zip_entry
        
    @property
    def assignment(self): 
        return self._match['assessment']
        
    @property
    def student_id(self): 
        return self._match['username']
    
    @property
    def filename(self):
        return self._match['filename'] or self._meta_file_name
    
    @property
    def is_metadata(self):
        return self._student_name is not None
    
    @property
    def student_name(self):
        return self._student_name
    
    @property
    def time(self):
        return time.strptime(self._match['timestamp'], '%Y-%m-%d-%H-%M-%S')
    
    @property
    def datetime(self):
        return datetime.datetime(*self.time[:6])
            
    def extract(self, path: str, timestamp: int = None):
        self._zip_reader.extract(self._zip_entry, path)
        new_filename = os.path.join(path, self.filename)
        os.rename(os.path.join(path, self.mangled_name), new_filename)
        if timestamp:
            os.utime(new_filename, times = (timestamp, timestamp))            
        return new_filename
            
    @property
    def text(self):
        return io.TextIOWrapper(
            self._zip_reader.open(self._zip_entry), 
            encoding='utf-8'
        ).read()
          
@contextlib.contextmanager
def open_grade_book(filename: str, *, meta_file_name: str):
    with zipfile.ZipFile(filename, 'r') as reader:
        def get_entries():
            for zip_entry in reader.infolist():
                yield GradeBookFileEntry(zip_entry, reader, meta_file_name)
                
        yield get_entries()

