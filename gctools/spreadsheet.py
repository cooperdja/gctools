import csv
import xml.etree.ElementTree as ElementTree
from zipfile import ZipFile
import re
import collections

class Row(tuple):
    def __new__(cls, index, data):
        return tuple.__new__(cls, data)
        
    def __init__(self, index, data):
        tuple.__init__(self)
        self.index = index
    
    def __getitem__(self, key):
        try:
            return tuple.__getitem__(self, key)
        except IndexError:
            return None
        
    def get_index(self):
        return self.index
        

class Spreadsheet:
    def which_cell(self, pattern, first_row = 0, first_column = 0):
        regex = re.compile(pattern, re.IGNORECASE)

        for row_n, row in enumerate(self.rows[first_row:], start = first_row):
            for colN, cell in enumerate(row[first_column:], start = first_column):
                if cell:
                    cell_str = str(cell)
                    if cell_str[0] in '\'"' and cell_str[-1] == cell_str[0]:
                        cell_str = cell_str[1:-1]
                    if regex.match(cell_str):
                        return row_n, colN
                
        raise SpreadsheetFormatException(f'No such pattern found (starting at R{first_row}C{first_column}): "{pattern}"')
    #/which_cell

    def which_cell_list(self, *pattern_list):
        row_n = -1
        colN = 0
        for pattern in pattern_list:
            try:
                row_n, colN = self.which_cell(pattern, first_row = row_n + 1, first_column = colN)
                
            except SpreadsheetFormatException as ex:
                raise SpreadsheetFormatException("No such pattern list found: '{}'".format(repr(pattern_list)))
            
        return row_n, colN
    #/which_cell_list
    
    
    def index(self, header_pattern, value_matcher = None, index_type = str):
        self.index_column = self.which_cell(header_pattern)[1]
        self.row_map = collections.OrderedDict((index_type(row[self.index_column]), row)
                                              for row in self.rows 
                                              if value_matcher is None or value_matcher(row[self.index_column]))
    #/setIndex
    
    def get_cell(self, ref, row_offset = 0, col_offset = 0):
        return self.rows[ref[0] + row_offset][ref[1] + col_offset]
    
    def set_rows(self, rows):
        self.rows = tuple(rows)
        self.nCols = max(len(r) for r in self.rows)
        
    def get_rows(self):
        return self.rows
    
    def get_row(self, key):
        #return self.row_map[key]
        return self.row_map.get(key) or self.row_map.get(key.lstrip('0'))
    
    def get_abs_row(self, row_n):
        return self.rows[row_n]
    
    def __getitem__(self, key):
        if isinstance(key, int):
            return self.rows[key]
        
        elif isinstance(key, str):
            #return self.row_map[key]
            return self.get_row(key)
        
        else:
            raise KeyError
        
        
    def get_indexes(self):
        return iter(self.row_map)
    
    def get_index_column_number(self):
        return self.index_column
    
    def get_data_row_numbers(self):
        return (i for i, row in enumerate(self.rows) if row[self.index_column] in self.row_map)
    


class CSVSpreadsheet(Spreadsheet):
    ENCODINGS = ['utf-8', 'utf-16']

    def __init__(self, filename):
        super().__init__()

        for encoding in self.ENCODINGS:
            try:
                lines = open(filename, encoding=encoding).readlines()
                break
            except ValueError:
                pass 
        else:
            raise SpreadsheetFormatException(f'Cannot decode {filename} as any of {self.ENCODINGS}')
        
        # Blackboard adds a UTF marker even if it isn't needed, so get rid of that:
        if lines[0].startswith('\ufeff'):
            lines[0] = lines[0][1:]

        self.set_rows(Row(i, r) for i, r in enumerate(csv.reader(lines)))


class ODSSpreadsheet(Spreadsheet):
    NAMESPACES = dict(
        office = "urn:oasis:names:tc:opendocument:xmlns:office:1.0",
        table  = "urn:oasis:names:tc:opendocument:xmlns:table:1.0",
        text   = "urn:oasis:names:tc:opendocument:xmlns:text:1.0"
    )
    
    N_COLS_REPEATED = "{urn:oasis:names:tc:opendocument:xmlns:table:1.0}number-columns-repeated"
        
    
    def generate_cells(self, xml_row):
        for xml_cell in xml_row.iterfind("*", self.NAMESPACES):
            text = xml_cell.findtext("*", "")
            for i in range(int(xml_cell.get(self.N_COLS_REPEATED, "1"))):
                yield text
    
    def __init__(self, filename):
        super().__init__()
        with ZipFile(filename, "r") as zipfile:
            tree = ElementTree.parse(zipfile.open("content.xml"))
            table = tree.find(".//table:table", self.NAMESPACES)
            self.set_rows(
                Row(i, self.generate_cells(xml_row))
                for i, xml_row in enumerate(table.iterfind(".//table:table-row", self.NAMESPACES))
            )


class SpreadsheetFormatException(Exception):
    pass


def read_spreadsheet(filename):
    filename_lc = filename.lower()
    if filename_lc.endswith('.csv'):
        return CSVSpreadsheet(filename)
    elif filename_lc.endswith('.ods'):
        return ODSSpreadsheet(filename)
    else:
        raise SpreadsheetFormatException(f'"{filename}" does not have a recognised extension')
