#!/usr/bin/python3

# Gcunpack: sensibly unpacks a Blackboard-generated zip file containing assignment submissions. See README.md.

import argparse
import re
from abc import ABC, abstractmethod
from typing import Dict, Tuple

import os
import os.path
import zipfile
import tarfile
import subprocess
import time
import datetime

from . import gradebook
from . import spreadsheet
from . import messages

DEFAULT_METAFILE = 'submission_info.txt'
DEFAULT_STUDENT_DIR_FORMAT = '%l%f_%i'

# A regex for filtering out characters that would be illegal in either Windows or Linux:
# (https://docs.microsoft.com/en-us/windows/win32/fileio/naming-a-file)
ILLEGAL_FILENAME_CHARS = '[<>:"/\\\\|?*.\x00-\x1f]'
REPLACEMENT_CHAR = '+'

FILENAME_WIDTH = 30
    
        
        
class NameMap(ABC):
    FORMAT_SPEC = re.compile(r'%%|%(.?)([ilf])')
    
    def __init__(self, format_str: str):
        self._format_str = format_str
    
    def __getitem__(self, student_id: str) -> str:
        last_name, first_name = self.get_name(student_id)
        return self.FORMAT_SPEC.sub(
            lambda match: 
                '%' if match[0] == '%%' else {
                    'i': student_id,
                    'l': match[1].join(last_name.split()),
                    'f': match[1].join(first_name.split()),
                    }[match[2]],
            self._format_str) 
    
    @abstractmethod
    def get_name(self, student_id: str) -> Tuple[str, str]: 
        raise NotImplementedError


class GcNameMap(NameMap):    
    def __init__(self, format_str: str, files: Dict[str, gradebook.GradeBookFileEntry]):
        super().__init__(format_str)
        self._files = files
        #print(f'format_str = {format_str}, files = {files}')
    
    def get_name(self, student_id: str) -> Tuple[str, str]:
        return (self._files[student_id].student_name, '')
    
    
class NameException(Exception):
    def __init__(self):
        super().__init__('Spreadsheet does not contain ID')
    
    
class SpreadsheetNameMap(NameMap):
    def __init__(self, format_str: str, ss_filename: str):
        super().__init__(format_str)
        self._sheet = spreadsheet.read_spreadsheet(ss_filename)
        self._sheet.index(r'username|(student|std) (id|no|num|#)|id (no|num|#)|(^\d{8}$)')
        
        _, self._last_name_column = self._sheet.which_cell(r'(student|stu)? ?(last|family|sur) ?name')
        _, self._first_name_column = self._sheet.which_cell(r'((pref?)erred)? ?(student|stu)? ?(first|1st|given) ?name')
        
    def get_name(self, student_id: str) -> Tuple[str, str]:
        row = self._sheet.get_row(student_id)
        if row is None:
            raise NameException()
        return (row[self._last_name_column], row[self._first_name_column])
        
        
class MangledArchiveException(Exception):
    def __init__(self, msg):
        super().__init__(f'Mangled/malicious archive -- {msg}. Manual extraction recommended.')
            
# Perform some sanity checks on a set of pathnames, as found in an archive.
def verify_paths(path_iter):
    path_list = list(path_iter)
    if len(path_list) != len(set(path_list)):
        raise MangledArchiveException('duplicate pathname(s) detected')

    for path in path_list:
        if len(path) == 0:
            raise MangledArchiveException('empty pathname(s) detected')
            
        elif re.match('[/\\\\]', path):
            raise MangledArchiveException(f'absolute pathname(s) detected: "{path}"')
        
        elif re.search('(^|/|\\\\)\.\.($|/|\\\\)', path):
            raise MangledArchiveException(f'pathname(s) containing the parent directory detected: "{path}"')


class UnpackCommandException(Exception):
    pass

def unpack_command(command_prefix, new_filename, path):
    try:
        subprocess.run(
            command_prefix + [os.path.basename(new_filename)], 
            cwd = path, 
            check = True,
            #capture_output = True, # Not supported in Python 3.6; use the following two lines instead.
            stdout = subprocess.PIPE,
            stderr = subprocess.PIPE,
        )
        os.remove(new_filename)
            
    except subprocess.CalledProcessError as proc_error:
        output = proc_error.output.decode('utf-8', errors='ignore')
        raise UnpackCommandException(f'"{command_prefix[0]}" command returned exit code {proc_error.returncode} and output:\n{output}\n') from proc_error
            
    except OSError as proc_error: 
        raise UnpackCommandException(f'Could not run "{command_prefix[0]}": {proc_error}') from proc_error
    

def main():                      
    arg_parser = argparse.ArgumentParser(description = "Sensibly unpacks a Blackboard-generated zip file containing assignment submissions.")
    arg_parser.add_argument(
        'zipfile', 
        metavar = 'zipfile', 
        type = str, 
        help = 'A \'gradebook-###.zip\' file downloaded from Blackboard\'s Grade Centre.')
    
    arg_parser.add_argument(
        '-d', '--dest',
        type = str,
        help = 'Specify the destination directory for the unpacked submissions, which will be created if necessary. By default, the destination directory name is taken from the name of the assignment, as implied by the gradebook .zip file.')

    arg_parser.add_argument(
        '-f', '--format',
        dest = 'format',
        type = str,
        default = DEFAULT_STUDENT_DIR_FORMAT,
        help = 'The format for submission directory names. The format string can contain "%%i", "%%?l" and/or "%%?f", which will be replaced by the student ID, last name and first name, respectively. "%%%%" will be replaced by a single "%%". In place of "?" can be any character; this will be used to separate individual words in each part of the name. If the word-separator character is omitted, the parts of the name will not be separated. All other characters in the format string are treated literally. The default format is "%%l%%f_%%i". Note: if the -s argument is not provided, "%%l" will instead represent the whole name in order, and "%%f" will be empty.')
    
    arg_parser.add_argument(
        '-s', '--spreadsheet',
        dest = 'spreadsheet',
        type = str,
        help = 'A spreadsheet file in which to find students\' last and first names; needed if you want control over how first names and last names appear in the extracted directories. (The zipfile itself contains students\' full names, but these cannot reliably be separated into last/first names in the event of multi-word names.)')
    
    arg_parser.add_argument(
        '-m', '--metafile',
        dest = 'meta_file_name',
        type = str,
        default = DEFAULT_METAFILE,
        help = f'The filename to use for each submission\'s metadata. These files are taken directly from the gradebook zip file, though that original format does not give them meaningful names. By default, gcunpack uses the name "{DEFAULT_METAFILE}".')
    
    arg_parser.add_argument(
        '-M', '--no-metafiles',
        action = 'store_true',
        help = f'Do not unpack metadata files ("{DEFAULT_METAFILE}" by default); only unpack the student submissions themselves.'
    )
    
    if 'fromisoformat' in dir(datetime.datetime):
        fromisoformat = datetime.datetime.fromisoformat        
    else:
        # Polyfill for Python 3.6
        fromisoformat = lambda s: datetime.strptime(s, '%Y-%m-%d-%H:%M:%S')
    
    arg_parser.add_argument(
        '-n', '--newer-than',
        metavar = 'YYYY-MM-DD[-HH[:MM[:SS]]]',
        type = fromisoformat,
        help = f'Only unpack submissions strictly newer than the given date (in ISO format). For archive (.zip, etc) files submitted after this date, the files inside could still have an older timestamp, but will still be included.')
    
    arg_parser.add_argument(
        '-o', '--older-than',
        metavar = 'YYYY-MM-DD[-HH[:MM[:SS]]]',
        type = fromisoformat,
        help = f'Only unpack submissions strictly older than the given date (in ISO format). The -n and -o options may be given together, but make sure to specify an overlapping range.')
    
    arg_parser.add_argument(
        '-Z', '--no-further-unzipping',
        action = 'store_true',
        help = 'Do not unzip/untar/etc. any archive files contained within the gradebook .zip file; simply write them out as-is. Normally, gcunpack will automatically attempt a second level of unzipping where possible.')
                            

    args = arg_parser.parse_args()
    
    newer_than = args.newer_than and args.newer_than.timestamp()
    older_than = args.older_than and args.older_than.timestamp()

    big_task = messages.Task().show('Extracting...')
    with gradebook.open_grade_book(args.zipfile, meta_file_name = args.meta_file_name) as gb_reader:
        files = []
        metadata_files = {}
        latest = {}
        
        for f in gb_reader:
            files.append(f)
            
            if f.is_metadata:
                metadata_files[f.student_id] = f
                
            latest_key = (f.assignment, f.student_id)
            if latest_key not in latest or latest[latest_key] < f.time:
                latest[latest_key] = f.time
                    
        name_map = (
            GcNameMap(args.format, metadata_files) if args.spreadsheet is None 
            else SpreadsheetNameMap(args.format, args.spreadsheet))

        time_map = {}

        # Now that we have all the metadata, we re-iterate through the files, extract each one to the appropriate directory, and rename it back to the original non-mangled name (as uploaded by the student). We build the directory structure as needed. If any of the files-to-be-extracted is itself a .zip, .tar.gz or .tar.bz2, we perform another level of extraction.
        
        name_failures = set()
        problem_files = []

        skipped_submissions_too_old = set()
        skipped_submissions_too_new = set()
        
        for f in files:
            # Filter out meta files, if asked.
            if f.is_metadata and args.no_metafiles:
                continue

            current_time = time.mktime(f.time)
            file_task = big_task.subtask().show(
                f'{f.filename.ljust(FILENAME_WIDTH)} (id={f.student_id}, time={f.datetime})')

            # Filter by timestamps, if asked.
            if newer_than and current_time <= newer_than:
                skipped_submissions_too_old.add((f.assignment, f.student_id, current_time))
                file_task.warning('Skipped because too old')
                continue
            
            if older_than and current_time >= older_than:
                skipped_submissions_too_new.add((f.assignment, f.student_id, current_time))
                file_task.warning('Skipped because too new')
                continue
                    
            
            # Create the principal directory for the assessment (if needed).
            dest_dir = args.dest or f.assignment
            if not os.path.isdir(dest_dir): 
                os.mkdir(dest_dir)
                
            # Determine what the directory for this file should be.
            try:
                name = name_map[f.student_id]
            except NameException as e:
                name = f.student_id
                if f.student_id not in name_failures:
                    name_failures.add(f.student_id)
                    file_task.error(f'Cannot get student name for ID {f.student_id}: {e}')
                
            path = os.path.join(
                re.sub(ILLEGAL_FILENAME_CHARS, REPLACEMENT_CHAR, dest_dir),
                re.sub(ILLEGAL_FILENAME_CHARS, REPLACEMENT_CHAR, name)
            )
            
            # Create the directory if it doesn't already exist.
            latest_time = time.mktime(latest[(f.assignment, f.student_id)])
            if not os.path.isdir(path):
                os.mkdir(path)
                time_map[path] = latest_time

            # If this file is part of an earlier (non-final) submission, relegate it to another (nested) directory for that earlier submission.
            if latest_time != current_time:
                path = os.path.join(path, time.strftime('%Y-%m-%d_%H%M%S', f.time))
                if not os.path.isdir(path):
                    os.mkdir(path)
                    time_map[path] = current_time
                    
            # Extract the file into whatever directory we decided.
            new_filename = f.extract(path, current_time)
            
            # If the file was itself an archive, extract it (if we can, and if the user hasn't 
            # turned this feature off).
            if not args.no_further_unzipping:
                try:
                    if re.match('.*\.zip', f.filename, re.IGNORECASE):
                        file_task.subtask().show('Extracting contents of ZIP archive')
                        with zipfile.ZipFile(new_filename, 'r') as archive:
                            verify_paths(archive.namelist())
                            archive.extractall(path)      
                            
                            # ZipFile.extractall does not preserve timestamps for some reason (and 
                            # sometimes .zip timestamps are absent anyway). So we'll set the time for
                            # each file one by one.
                            for entry in archive.infolist():
                                try:
                                    file_time = datetime.datetime(*entry.date_time).timestamp()
                                except ValueError:
                                    # Invalid timestamp; use overall submission time instead:
                                    file_time = current_time
                                else:
                                    os.utime(os.path.join(path, entry.filename), (file_time, file_time))
                            
                        os.remove(new_filename)
                    
                    elif re.match('.*\.(tar(\.gz|\.bz2|\.xz)?|tgz|tbz2|txz)', f.filename, re.IGNORECASE):
                        file_task.subtask().show('Extracting contents of TAR archive')
                        with tarfile.open(new_filename) as archive:
                            verify_paths(archive.getnames())
                            archive.extractall(path)
                        os.remove(new_filename)
                        
                    elif re.match('.*\.rar', f.filename, re.IGNORECASE):
                        file_task.subtask().show('Extracting contents of RAR archive')
                        unpack_command(['unrar', '-y', 'x'], new_filename, path)

                    elif re.match('.*\.7z', f.filename, re.IGNORECASE):
                        file_task.subtask().show('Extracting contents of 7Z archive')
                        unpack_command(['7z', '-y', 'x'], new_filename, path)
                                                
                    #elif re.match('.*\.(rar|7z|tar\.[a-zA-Z0-9]+|tz)', f.filename, re.IGNORECASE):
                        #file_task.error('This looks like an archive that gcunpack doesn\'t support. You will need to extract it yourself!\n')
                        #problem_files.append(new_filename)
                    
                except Exception as archiveError:
                    problem_files.append(new_filename)
                    file_task.error(f'Attempted to extract archive, but: {archiveError}')
                    # Keep going on the remaining files.        
                    try:
                        file_task.error('It\'s actually... ' + 
                            subprocess.check_output(['file', '-b', new_filename]).decode())
                        
                    except subprocess.CalledProcessError as proc_error:
                        file_task.error(f'Sorry, tried and failed to determine actual file type: {proc_error}')
                        
                    except OSError as proc_error:
                        file_task.error(f'Sorry, tried and failed to determine actual file type: {proc_error}')
        #end-for
        
        
        # Correct timestamp on directories. This has to be done *after* all the directory contents are in place, or else the timestamps will be reset.
        for path in time_map.keys():
            os.utime(path, times = (time_map[path], time_map[path]))
        
        
    #end-with
    
    if len(skipped_submissions_too_old) > 0:
        big_task.warning(f'{len(skipped_submissions_too_old)} submission(s) skipped, because their time(s) were before (or equal to) {args.newer_than}')

    if len(skipped_submissions_too_new) > 0:
        big_task.warning(f'{len(skipped_submissions_too_new)} submission(s) skipped, because their time(s) were after (or equal to) {args.older_than}')

    if len(name_failures) > 0:
        print()
        big_task.error('There were problems obtaining a student name for the following IDs:')
        e = big_task.subtask()
        for id in name_failures:
            e.subtask().show(id)

    if len(problem_files) > 0:
        print()
        big_task.error('There were problems with the following files:')
        e = big_task.subtask()
        for f in problem_files:
            e.subtask().show(f)

    # Done!


if __name__ == '__main__':
    main()
    
