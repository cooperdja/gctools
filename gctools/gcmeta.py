#!/usr/bin/python3

import argparse
import csv
import datetime
import re

from . import gradebook
from . import messages

PATTERN = re.compile(
    '''
        Name: \s* (?P<name>[^\\n]+) \s*\( (?P<id>[^)]+) \)\\n
        Assignment: \s* (?P<assessment>[^\\n]+) \\n
        Date\sSubmitted: \s* (?P<time>[^\\n]+) \\n
        Current\sMark: \s* (?P<mark>[^\\n]+) \\n
        (Override\sMark: \s* (?P<overridemark>[^\\n]+) \\n)?
        \\n
        Submission\sField: \s*\\n (?P<submissionField>.*) \\n\\n
        Comments: \s*\\n (?P<comments>.*) \\n\\n
        Files: \s*\n (?P<files>.*)
    ''', 
    re.DOTALL | re.VERBOSE
)

FILE_PATTERN = re.compile(
    '''
        \s* Original\sfilename: \s* (?P<filename>[^\\n]+) \\n
        \s* Filename: \s* [^\\n]* \\n
    ''',
    re.VERBOSE
)


EMPTY_SUBMISSION_FIELD = 'There is no student submission text data for this assignment.'
EMPTY_COMMENTS         = 'There are no student comments for this assignment.'

class MetadataError(Exception):
    def __init__(self, gb_file: gradebook.GradeBookFileEntry, msg: str = ''):
        super().__init__(msg)
        self.filename = gb_file.mangled_name
        self.text = gb_file.text
    

def extractMetadata(gb_file: str, csv_file: str):
    METAFILE = 'metafile.txt' # Name not important here
    entries = []
    n_submissions = {}
    
    with gradebook.open_grade_book(gb_file, meta_file_name = METAFILE) as gb_reader:
        for f in gb_reader:
            if f.is_metadata:
                match = PATTERN.fullmatch(f.text)
                if not match:
                    raise MetadataError(f)
                
                try:
                    t = match['time']
                    submission_time = datetime.datetime.strptime(t, '%A, %d %B %Y %H:%M:%S o\'clock %Z')
                    
                except ValueError as e:
                    raise MetadataError(f, f'Unrecognised time format: "{t}".') from e
                
                id = match['id']
                entries.append((id, submission_time, match))                
                n_submissions[id] = n_submissions.get(id, 0) + 1
                
        #end-for
    #end-with
                
    entries.sort()
    
    with open(csv_file, 'w') as csv_file_writer:
        csv_writer = csv.writer(csv_file_writer)
        csv_writer.writerow([
            'Student ID', 'Student Name', 'Assessment', 'Final?', 'Submission #', 
            'Submission Time', 'Mark', 'Override Mark', 'Submission Field', 'Comments', 'Files'])
    
        prev_id = ''
        for id, submission_time, match in entries:
            if id == prev_id:
                submission_n += 1
            else:
                submission_n = 1
            prev_id = id
                
            files = '\n'.join(fmatch['filename'].strip() for fmatch in FILE_PATTERN.finditer(match['files']))
            
            csv_writer.writerow([
                match['id'].strip(),
                match['name'].strip(),
                match['assessment'].strip(),
                'TRUE' if submission_n == n_submissions[id] else 'FALSE',
                f'{submission_n} of {n_submissions[id]}',
                submission_time.isoformat(sep=' '),
                match['mark'].strip(),
                (match['overridemark'] or '').strip(),
                match['submissionField'].strip() if match['submissionField'] != EMPTY_SUBMISSION_FIELD else '',
                match['comments'].strip()        if match['comments']        != EMPTY_COMMENTS         else '',
                files
            ])
        #end-for
    #end-with
            
            

def file_type(ext: str):
    def validator(filename):
        if not filename.endswith('.' + ext):
            raise TypeError
        return filename
    
    return validator


def main():
    arg_parser = argparse.ArgumentParser(
        description = 'Extracts metadata from a Blackboard "gradebook-###.zip" file and writes it to a CSV.')
    
    arg_parser.add_argument(
        'gb_file', 
        metavar = 'gradebook-###.zip', 
        type = file_type('zip'), 
        help = 'A "gradebook-###.zip" file downloaded from Blackboard\'s Grade Centre.')
    
    arg_parser.add_argument(
        'csv_file', 
        metavar = 'metadata.csv', 
        type = file_type('csv'), 
        nargs = '?',
        help = 'The name of the CSV file to create.')

    args = arg_parser.parse_args()
    
    try:
        extractMetadata(args.gb_file, args.csv_file or f'{args.gb_file[:-3]}csv')
        
    except MetadataError as e:
        messages.error(f'Metadata file {e.filename} does not conform to expected pattern. {e}\n---\n{e.text}\n---')


if __name__ == '__main__':
    main()
